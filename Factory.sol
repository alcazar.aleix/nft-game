import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

pragma solidity ^0.8.10;

interface INFT {
    function preFusion( uint256 _nftId1, uint256 _nftId2, address player)external returns(bool);
    function mintProperty(uint256 random, address player) external returns (uint256);
    function mintKit(uint256 typeKit,address player) external returns (uint256);
    function mintSeed(uint256 random, address player) external returns (uint256);
    function safeBatchTransferFrom(
        address _from,
        address _to,
        uint256[] calldata _ids,
        uint256[] calldata _values,
        bytes calldata _data
    ) external;
    function balanceOfBatch(
    address[] calldata _owners,
    uint256[] calldata _ids
    ) external view returns (uint256[] memory);
    function balanceOf(address account, uint256 id) external view returns (uint256);
    function balancePerOwner(address _player) external view returns(uint256[] memory);

}

pragma solidity ^0.8.10;

interface INFTRepository {
    struct Props{
        address nftOwner;
        string name;
        uint256 id; 
        uint256 capacity;
        uint256 duration;
        uint256 levelId;
    }
    function add(uint256 id, Props memory _props) external;

    function remove(uint256 id) external;

    function get(uint256 id) external view returns(Props memory _props);

}
interface INFTPropertys {
    struct Props{
        address nftOwner;
        string name;
        uint256 id; 
        uint256 capacity;
        uint256 duration;
        uint256 levelId;
    }
    function deletePropertysByCounterId(uint256 _counterId)external;
    function getPropertysByCounterId(uint256 id) external view returns(Props memory);
    function getPropertys() external view returns(Props[] memory);
    function mintProperty(uint256 random, address player) external returns (uint256);
    function mintKit(uint256 typeKit,address player) external returns (uint256);
    function mintSeed(uint256 random, address player) external returns (uint256);
}



interface IFeeToken is IERC20 {

    function decimals() external view returns (uint8);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

}
interface IMarketPlaceBUD {

    function checkSellers(uint256 _nftID)public returns(bool);

}



pragma solidity ^0.8.10;

contract Factory is Pausable, Ownable {
    using SafeMath for uint256;

    INFT public nft;
    INFTRepository public nftRepository;
    INFTPropertys public nftPropertys;
    IMarketPlaceBUD public marketPlace;
    uint256 public feeWei;
    uint256 public fee;
    address public poolRewardsAddress;
    IFeeToken public feeToken;
    //mapping relation token_id => address_owner => name NFT
    mapping (uint256 => mapping(address => string)) public _holderType;
    //mapping relation address_owner => token_id 
    mapping (address => uint256) public _holders;


    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function pause() public virtual onlyOwner whenNotPaused {
        _pause();
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function unpause() public virtual onlyOwner whenPaused {
        _unpause();
    }


    function setNFT(address addr) external onlyOwner {
        nft = INFT(addr);
    }

    function setNFTRepository(address addr) external onlyOwner {
        nftRepository = INFTRepository(addr);
    }
    function setNFTPropertys(address addr) external onlyOwner {
        nftPropertys = INFTPropertys(addr);
    }

    function setFee(uint256 number) external onlyOwner {
        fee = number;
        feeWei = fee * 10 ** uint(feeToken.decimals());
    }

    function setpoolRewardsAddress(address addr) external onlyOwner {
        poolRewardsAddress = addr;
    }

    function setFeeToken(address addr) external onlyOwner {
        feeToken = IFeeToken(addr);
    }
    function getIDByOwner(address _owner) external view returns(uint256){
        return _holders[_owner];
    }
 
    function _build(uint decision, uint256 _randonGen) external whenNotPaused returns(uint256) {
        uint256 id;
        uint256 id_prop;
        if(decision == 0){

            //Mint NFT
            id = nft.mintProperty(_randonGen,_msgSender());
            //Crete Propertys to NFT minted 
            id_prop = nftPropertys.mintProperty(_randonGen,_msgSender());
        }else if(decision == 1){
            id = nft.mintKit(1,_msgSender());
            id_prop = nftPropertys.mintKit(1,_msgSender());
        }else if(decision == 2){
            id = nft.mintSeed(_randonGen,_msgSender());
            id_prop = nftPropertys.mintSeed(_randonGen,_msgSender());
        }
        
        INFTPropertys.Props memory createdProp = nftPropertys.getPropertysByCounterId(id);
        //Associate created propertys to NFT id and Add to repository
        INFTRepository.Props memory castedProp = INFTRepository.Props(createdProp.nftOwner,createdProp.name,createdProp.id,createdProp.capacity, createdProp.duration, createdProp.levelId);
        _holderType[id][msg.sender] = createdProp.name;
        _holders[msg.sender] = id;
        // Activate in production => preapprove in Dapp
        feeToken.transferFrom(msg.sender, poolRewardsAddress, feeWei);

        nftRepository.add(id, castedProp);
        return id;
    }
    function getFee() external view returns(uint256){
        return feeWei;
    }
    function fusionKits( uint256 _nftId1,uint256 _nftId2)external whenNotPaused returns(uint256){
        //This send NFT to deposit
        require(!marketPlace.checkSellers(_nftId1),"This NFT is on MarketPlace, please remove before");
        require(!marketPlace.checkSellers(_nftId2),"This NFT is on MarketPlace, please remove before");
        INFTPropertys.Props memory nftProp1 =  nftPropertys.getPropertysByCounterId(_nftId1);
        INFTPropertys.Props memory nftProp2 =  nftPropertys.getPropertysByCounterId(_nftId2);
        require(nftProp1.levelId == nftProp2.levelId,"Only NFT the same level can be combined!");
        nft.preFusion(_nftId1,_nftId2,_msgSender());
        //Remove NFT from proerty and Repository
        nftRepository.remove(_nftId1);
        nftRepository.remove(_nftId2);
        nftPropertys.deletePropertysByCounterId(_nftId1);
        nftPropertys.deletePropertysByCounterId(_nftId2);
        delete _holderType[_nftId1][msg.sender];
        delete _holderType[_nftId2][msg.sender];
        uint[] memory ids = new uint[](2);
        address[] memory addrss = new address[](2);
        ids[0] = _nftId1;
        ids[1] = _nftId2;
        addrss[0] = msg.sender;
        addrss[1] = msg.sender;
        require(nft.balanceOf( msg.sender, _nftId1) == 0, "No deposited Previous NFT fail execution ");
        require(nft.balanceOf( msg.sender, _nftId2) == 0, "No deposited Previous NFT fail execution ");
        feeToken.transferFrom(msg.sender, poolRewardsAddress, feeWei);
        nftPropertys.mintKit((nftProp1.levelId - 10) + 1,msg.sender);
        uint256 id =  nft.mintKit((nftProp1.levelId - 10) + 1,msg.sender);
        INFTPropertys.Props memory createdProp = nftPropertys.getPropertysByCounterId(id);
        INFTRepository.Props memory castedProp = INFTRepository.Props(createdProp.nftOwner,createdProp.name,createdProp.id,createdProp.capacity, createdProp.duration, createdProp.levelId);
        nftRepository.add(id, castedProp);
        return id;

    }
    function fusionSeeds(uint256 _seedId1,uint256 _seedId2)external whenNotPaused returns(uint256){
        require(!marketPlace.checkSellers(_nftId1),"This NFT is on MarketPlace, please remove before");
        require(!marketPlace.checkSellers(_nftId2),"This NFT is on MarketPlace, please remove before");
        //This send NFT to deposit
        INFTPropertys.Props memory nftProp1 =  nftPropertys.getPropertysByCounterId(_seedId1);
        INFTPropertys.Props memory nftProp2 =  nftPropertys.getPropertysByCounterId(_seedId2);
        require(nftProp1.levelId == nftProp2.levelId,"Only NFT the same level can be combined!");
        nft.preFusion(_seedId1,_seedId2,_msgSender());
        //Remove NFT from proerty and Repository
        nftRepository.remove(_seedId1);
        nftRepository.remove(_seedId2);
        nftPropertys.deletePropertysByCounterId(_seedId1);
        nftPropertys.deletePropertysByCounterId(_seedId2);
        delete _holderType[_seedId2][_msgSender()];
        delete _holderType[_seedId2][_msgSender()];
        uint[] memory ids = new uint[](2);
        address[] memory addrss = new address[](2);
        ids[0] = _seedId1;
        ids[1] = _seedId2;
        addrss[0] = msg.sender;
        addrss[1] = msg.sender;

        require(nft.balanceOf( msg.sender, _seedId1) == 0, "No deposited Previous NFT fail execution ");
        require(nft.balanceOf( msg.sender, _seedId2) == 0, "No deposited Previous NFT fail execution ");
        feeToken.transferFrom(msg.sender, poolRewardsAddress, feeWei);
        nftPropertys.mintSeed((nftProp1.levelId - 20) + 1,msg.sender);
        uint256 id =  nft.mintSeed((nftProp1.levelId - 20) + 1,msg.sender);
        INFTPropertys.Props memory createdProp = nftPropertys.getPropertysByCounterId(id);
        INFTRepository.Props memory castedProp = INFTRepository.Props(createdProp.nftOwner,createdProp.name,createdProp.id,createdProp.capacity, createdProp.duration, createdProp.levelId);
        nftRepository.add(id, castedProp);
        return id;
    }
    function balancePerOwner(address _player) external view returns(uint256[] memory){
        return (nft.balancePerOwner(_player));
    }

}