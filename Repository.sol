import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
pragma solidity ^0.8.10;

interface IRepository {
    struct Props{
        address nftOwner;
        string name;
        uint256 id; 
        uint256 capacity;
        uint256 duration;
        uint256 levelId;
    }
    function add(uint256 id,Props memory _props) external;

    function remove(uint256 id) external;

    function updateProps(uint256 id,Props memory _props) external;

    function get(uint256 id) external view returns(Props memory _props);

}

pragma solidity ^0.8.10;

contract Governances is Ownable {

    mapping(address => bool) public governances;

    function addGovernance(address governance) public onlyOwner {
        governances[governance] = true;
    }

    function removeGovernance(address governance) public onlyOwner {
        governances[governance] = false;
    }

    modifier onlyGovernance() {
        require(governances[_msgSender()], "ERC721: caller is not the owner");
        _;
    }

}
interface INFT {
    
    function balanceOf(address account, uint256 id) external view returns (uint256);
    function balancePerOwner(address _player) external view returns(uint256[] memory);

}
pragma solidity ^0.8.10;

contract Repository is IRepository, Governances {
    Props[] public props;
    INFT public nft;
    mapping(uint256 => Props) private propsMapping;


    function add(uint256 id, Props memory _props) public onlyGovernance override {
        propsMapping[id] = _props;
    }
    function updateProps(uint256 id, Props memory _props) public onlyGovernance override {
        propsMapping[id]=_props;
        props[id] = _props;
    }


    function remove(uint256 id) public onlyGovernance override {
        delete propsMapping[id];
    }

    function get(uint256 id) public view override returns(Props memory) {
        Props storage prop = propsMapping[id];
        return prop;
    }
    function balancePerOwner(address _player) external view returns(uint256[] memory){
        return (nft.balancePerOwner(_player));
    }

}