pragma solidity 0.8.10;
//incluir ERC721 / 1165
import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
contract Governances is Ownable {

    mapping(address => bool) public governances;

    function addGovernance(address governance) public onlyOwner {
        governances[governance] = true;
    }

    function removeGovernance(address governance) public onlyOwner {
        governances[governance] = false;
    }

    modifier onlyGovernance() {
        require(governances[_msgSender()], "ERC721: caller is not the owner");
        _;
    }

}
contract Propertys is Ownable, Governances{
    //Propertys
    uint8 public constant CLOSET = 1;
    uint8 public constant LOFT = 2;
    uint8 public constant HOUSE = 3;
    uint8 public constant WAREHOUSE = 4;
    uint8 public constant NAIVE = 5;
    uint closet_capacity = 2;
    uint loft_capacity = 5;
    uint house_capacity = 10;
    uint warehouse_capacity = 18;
    uint naive_capacity = 25;
        //volem duracio als immobles?
    uint closet_duration = 13;
    uint loft_duration = 15;
    uint house_duration = 18;
    uint warehouse_duration = 20;
    uint naive_duration = 20;
    //Kits
    uint8 public constant LAMP150W = 11;
    uint8 public constant LAMP300W = 12;
    uint8 public constant LAMP600W = 13;
    uint8 public constant LAMPLED = 14;
    uint8 public constant LAMP1200W = 15;
    uint lamp150_capacity = 2;
    uint lamp300_capacity = 4;
    uint lamp600_capacity = 8;
    uint lampLed_capacity = 16;
    uint lamp1200_capacity = 24;
        //duracio lamparas
    uint lamp150_duration = 13;
    uint lamp300_duration = 15;
    uint lamp600_duration = 18;
    uint lampLed_duration = 20;
    uint lamp1200_duration = 20;
    //Seeds
    uint8 public constant SEED1 = 21;
    uint8 public constant SEED2 = 22;
    uint8 public constant SEED3 = 23;
    uint8 public constant SEED4 = 24;
    uint8 public constant SEED5 = 25;
        //duracio lamparas

    
    
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    mapping(address => bool) private _minters;
    mapping (uint256 => mapping(address => string)) public _holderType;
    //mapping relation address_owner => token_id 
    mapping (address => uint256) public _holders;
    mapping (uint256 => address) public _holdersRev;

    event PropertyMinted(address indexed receiver, string _name, uint256 _ids);

    struct Props{
        address nftOwner;
        string name;
        uint256 id; 
        uint256 capacity;
        uint256 duration;
        uint8 levelId;
    }
    Props[] public props;
    //helper


    //modifiers


    //constructor
    constructor(){
    }
    //funcions
    // function uri(uint _nftId)override public pure returns(string memory){
    //     return string(abi.encodePacked("ipfs://bafybeifygp6dfvrfdcqfsp46ldjth3hnknsxbvoup2xtb7k5xyefqifwu4/",Strings.toString(_nftId),".json"));
    // }
    function mintProperty( uint256 _randomProperty, address _player)external payable onlyGovernance returns (uint256){
            // require ( msg.value >= 0.1 ether);
            require(_randomProperty > 0 && _randomProperty < 6,"No valid random number");
            _tokenIds.increment();
            if(_randomProperty == 1){
                _mintCloset(1,_player);
            }else if(_randomProperty == 2){
                _mintLoft(1,_player);
            }else if(_randomProperty == 3){
                _mintHouse(1,_player);
            }else if(_randomProperty == 4){
                _mintWarehouse(1,_player);
            }else{
                _mintNaive(1,_player);
            }           
            
            _holders[msg.sender] = _tokenIds.current();
            _holdersRev[_tokenIds.current()] = msg.sender;
            return _tokenIds.current();
        }

    function _mintCloset(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player, "Generic_Closet", _tokenIds.current(), closet_capacity, closet_duration, CLOSET);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_Closet";
            props.push(newProp);
        }
    }
    function _mintLoft(uint256 amount,address player)internal{
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_Loft", _tokenIds.current(), loft_capacity, loft_duration, LOFT);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_Loft";
            props.push(newProp);
        }

    }
    function _mintHouse(uint256 amount,address player)internal {
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_House", _tokenIds.current(), house_capacity, house_duration, HOUSE);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_House";
            props.push(newProp);
        }
    }
    function _mintWarehouse(uint256 amount,address player)internal{
        for(uint i = 0; i < amount; i++){   
            Props memory newProp = Props(player,"Generic_Warehouse", _tokenIds.current(), warehouse_capacity, warehouse_duration, WAREHOUSE);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_Warehouse";
            props.push(newProp);
        }
    }
    
    function _mintNaive(uint256 amount,address player)internal{
        for(uint i = 0; i < amount; i++){   
            Props memory newProp = Props(player,"Generic_Naive", _tokenIds.current(), naive_capacity, naive_duration, NAIVE);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_Naive";
            props.push(newProp);
        }
    }

    
    // event PropertyMinted(address indexed receiver, string _name, Counters.Counter _ids);
    //variables
    function mintKit(uint _typeKit, address _player)external payable onlyGovernance returns (uint256){
         _tokenIds.increment();
        if(_typeKit == 1){
                _mintLamp150W(1,_player);
            }else if(_typeKit == 2){
                _mintLamp300W(1,_player);
            }else if(_typeKit == 3){
                _mintLamp600W(1,_player);
            }else if(_typeKit == 4){
                _mintLamp1200W(1,_player);
            }else{
                _mintLampLed(1,_player);
            }
            _holders[msg.sender] = _tokenIds.current();
            _holdersRev[_tokenIds.current()] = msg.sender;
            return _tokenIds.current();
        }
    function _mintLamp150W(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_LAMP150W", _tokenIds.current(), lamp150_capacity, lamp150_duration, LAMP150W);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_LAMP150W";
            props.push(newProp);
        }
    }
    function _mintLamp300W(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_LAMP300W", _tokenIds.current(), lamp300_capacity, lamp300_duration, LAMP300W);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_LAMP300W";
            props.push(newProp);
        }
    }

    function _mintLamp600W(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_LAMP600W", _tokenIds.current(), lamp600_capacity, lamp600_duration, LAMP600W);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_LAMP600W";
            props.push(newProp);
        }
    }
    function _mintLampLed(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_LAMPLed", _tokenIds.current(), lampLed_capacity, lampLed_duration, LAMPLED);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_LAMPLed";
            props.push(newProp);
        }
    }
    
    function _mintLamp1200W(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_LAMP1200W", _tokenIds.current(), lamp1200_capacity, lamp1200_duration, LAMP1200W);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_LAMP1200W";
            props.push(newProp);
        }
    }
    function mintSeed(uint256 randomNumber,address _player)external payable onlyGovernance returns(uint256){
        _tokenIds.increment();
        if(randomNumber == 1){
                _mintSeed1(1,_player);
            }else if(randomNumber == 2){
                _mintSeed2(1,_player);
            }else if(randomNumber == 3){
                _mintSeed3(1,_player);
            }else if(randomNumber == 4){
                _mintSeed4(1,_player);
            }else{
                _mintSeed5(1,_player);
            }
            _holders[msg.sender] = _tokenIds.current();
            _holdersRev[_tokenIds.current()] = msg.sender;
        return _tokenIds.current();
    }
    function _mintSeed1(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_SEED1", _tokenIds.current(), 0, 1, SEED1);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_SEED1";
            props.push(newProp);
        }
    }
    function _mintSeed2(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_SEED2", _tokenIds.current(), 0, 1, SEED2);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_SEED2";
            props.push(newProp);
        }
    }
    function _mintSeed3(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_SEED3", _tokenIds.current(), 0, 1, SEED3);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_SEED3";
            props.push(newProp);
        }
    }
    function _mintSeed4(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_SEED4", _tokenIds.current(), 0, 1, SEED4);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_SEED4";
            props.push(newProp);
        }
    }
    function _mintSeed5(uint256 amount,address player)internal{
        
        for(uint i = 0; i < amount; i++){
            Props memory newProp = Props(player,"Generic_SEED5", _tokenIds.current(), 0, 1, SEED5);
            _holderType[_tokenIds.current()][msg.sender] = "Generic_SEED5";
            props.push(newProp);
        }
    }
    function getPropertys() public view returns(Props[] memory){
        return props;
    }
    function getPropertysByCounterId(uint256 _counterId) public view returns(Props memory){
        return props[_counterId-1];
    }
    function updatePropertysByCounterId(uint256 _counterId) public view onlyGovernance returns(Props memory){
        return props[_counterId-1];
    }
    function deletePropertysByCounterId(uint256 _counterId) external onlyGovernance {
        delete props[_counterId-1];
        delete _holders[msg.sender];
        delete _holdersRev[_counterId];
    }
    function getIDsByOwner(address _owner) external returns (uint256){
        return _holders[_owner];
    }

}
